module ApplicationHelper
  def logged_in_user
    current_user
  end

  def session_button
    if current_user
      render partial: "partials/logout_link"
    else
      render partial: "partials/session_buttons"
    end
  end

  def monitor_alert
    render partial: "partials/monitor_link" unless current_user
  end

  def dashboard
    render partial: "partials/dashboard_link" if current_user
  end
end
