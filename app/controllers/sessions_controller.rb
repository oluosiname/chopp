class SessionsController < ApplicationController
  def create
    user = User.find_by_username(params[:username])
    if user && user.authenticate(params[:password])
      login user
      redirect_to root_path
    else
      flash["error"] = "Invalid email/password"
      redirect_to login_path
    end
  end

  def destroy
    logout
    redirect_to root_path
  end
end
